package pl.codementors;

import pl.codementors.models.*;

import java.io.*;


/**
 * ZOO main class
 *
 * @author Krzysztof Koper
 */
public class Zoo {

    /**
     * Application main method.
     *
     * @param args Command line arguments.
     */

    public static void main(String[] args) {

        System.out.println("Welcome to the ZOO!");
        Animal[] animals = fileLoad("/home/student/java/animals/animals.txt"); //reads animals from file & storing them in array
        fileWriter("/home/student/java/animals/animals2.txt", animals); //save to file from array
        printer(animals, "Readed from text file"); //print all from array
        feedAll(animals); //feeding every animal from array
        feedCarnivore(animals); //feed only carnivores
        feedHerbivore(animals); //feed only herbivores
        howling(animals); //forcing animal to make sound - only howling
        tweet(animals); //forcing animal to make sound - tweeting
        hiss(animals); //forcing animal to make sound - hissing
        writeBinary("/home/student/java/animals/animalsbinary.txt", animals);
        Animal[] animalsFromBinary = readBinary("/home/student/java/animals/animalsbinary.txt");
        printer(animalsFromBinary, "Readed from binary file");
    }

    /**
     * reading from file
     *
     * @param filePath put full file path here
     */
    private static Animal[] fileLoad(String filePath) {
        File file = new File(filePath);
        Animal[] animals = null;

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            int arrSize = Integer.parseInt(br.readLine());
            animals = new Animal[arrSize];
            int i = 0;
            while (br.ready()) {
                switch (br.readLine()) {

                    case "Wolf": {
                        String name = br.readLine();
                        int age = Integer.parseInt(br.readLine());
                        String color = br.readLine();
                        animals[i] = new Wolf(name, age, color);
                        i++;
                        break;
                    }
                    case "Iguana": {
                        String name = br.readLine();
                        int age = Integer.parseInt(br.readLine());
                        String color = br.readLine();
                        animals[i] = new Iguana(name, age, color);
                        i++;
                        break;
                    }
                    case "Parrot": {
                        String name = br.readLine();
                        int age = Integer.parseInt(br.readLine());
                        String color = br.readLine();
                        animals[i] = new Parrot(name, age, color);
                        i++;
                        break;
                    }
                }
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }
        return animals;
    }

    /**
     * writing array to file
     *
     * @param filePath enter full file path here
     * @param arrName  enter array which you want to write to text file
     */
    private static void fileWriter(String filePath, Animal[] arrName) {
        File file = new File(filePath);
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(file));
            bw.write(arrName.length + "\n");
            for (Animal anArrName : arrName) {
                bw.write(anArrName.getClass().getSimpleName());
                bw.write("\n");
                bw.write(anArrName.getName());
                bw.write("\n");
                bw.write(Integer.toString(anArrName.getAge()));
                bw.write("\n");
                if (anArrName instanceof Wolf) {
                    bw.write(((Wolf) anArrName).getFurColor());
                    bw.write("\n");
                } else if (anArrName instanceof Parrot) {
                    bw.write(((Parrot) anArrName).getFeatherColor());
                    bw.write("\n");
                } else if (anArrName instanceof Iguana) {
                    bw.write(((Iguana) anArrName).getScaleColor());
                    bw.write("\n");
                }

            }
        } catch (IOException ex) {
            System.err.println(ex);
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException ex) {
                    System.err.println(ex);
                }
            }
        }
    }

    /**
     * printing all animals from array
     *
     * @param arrName   enter array name of animals to print
     * @param firstLine prints this String in first line
     */
    private static void printer(Animal[] arrName, String firstLine) {
        System.out.println("------------------------------");
        System.out.println(firstLine);
        for (Animal anArrName : arrName) {
            System.out.println("name: " + anArrName.getName());
            System.out.println("age: " + anArrName.getAge());
            System.out.println("type: " + anArrName.getClass().getSimpleName());
            if (anArrName instanceof Wolf) {
                System.out.println("fur colour: " + ((Wolf) anArrName).getFurColor());
            } else if (anArrName instanceof Parrot) {
                System.out.println("feather colour: " + ((Parrot) anArrName).getFeatherColor());
            } else if (anArrName instanceof Iguana) {
                System.out.println("scale colour: " + ((Iguana) anArrName).getScaleColor());
            }
            System.out.println("##############################");
        }
    }

    /**
     * Feeding every animal
     *
     * @param arrName enter array name of animals to feed
     */
    private static void feedAll(Animal[] arrName) {
        System.out.println("------------------------------");
        System.out.println("Feeding every animal from array\n");
        for (Animal anArrName : arrName) {
            anArrName.eating();
        }
        System.out.println("##############################");
    }

    /**
     * Feeding only carnivore animals
     *
     * @param arrName enter array name of animals to feed
     */
    private static void feedCarnivore(Animal[] arrName) {
        System.out.println("------------------------------");
        System.out.println("Feeding only meat-eaters!\n");
        for (Animal anArrName : arrName) {
            if (anArrName instanceof Carnivore) {
                ((Carnivore) anArrName).meatEating();
            }
        }
        System.out.println("##############################");
    }

    /**
     * Feeding only herbivore animals
     *
     * @param arrName enter array name of animals to feed
     */
    private static void feedHerbivore(Animal[] arrName) {
        System.out.println("------------------------------");
        System.out.println("Feeding only green-eaters!\n");
        for (Animal anArrName : arrName) {
            if (anArrName instanceof Herbivore) {
                ((Herbivore) anArrName).eatingPlants();
            }
        }
        System.out.println("##############################");
    }

    /**
     * Forcing animal to howl
     *
     * @param arrName enter array name of animals which you want to force to howl.
     */
    private static void howling(Animal[] arrName) {
        for (Animal anArrName : arrName) {
            if (anArrName instanceof Wolf) {
                ((Wolf) anArrName).sound();
            }
        }
        System.out.println("##############################");
    }

    /**
     * Forcing animal to hissing
     *
     * @param arrName enter array name of animals which you want to force to hiss.
     */
    private static void hiss(Animal[] arrName) {
        for (Animal anArrName : arrName) {
            if (anArrName instanceof Iguana) {
                ((Iguana) anArrName).sound();
            }
        }
        System.out.println("##############################");
    }

    /**
     * Forcing animal to tweeting
     *
     * @param arrName enter array name of animals which you want to force to tweet
     */
    private static void tweet(Animal[] arrName) {
        for (Animal anArrName : arrName) {
            if (anArrName instanceof Parrot) {
                ((Parrot) anArrName).sound();
            }
        }
        System.out.println("##############################");
    }

    /**
     * Writes array signed as a parameter to binary file
     *
     * @param filePath enter full file path to file
     * @param arrName  array name which you want to write to binary file
     */
    private static void writeBinary(String filePath, Animal[] arrName) {
        File file = new File(filePath);
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(arrName);
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    /**
     * Reading from binary file
     *
     * @param filePath enter full path to file
     * @return array with Animals readed from binary file.
     */

    private static Animal[] readBinary(String filePath) {
        Animal[] animalsFromBinary = null;
        File file = new File(filePath);
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            animalsFromBinary = (Animal[]) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            System.err.println(ex);
        }
        return animalsFromBinary;
    }
}
