package pl.codementors.models;

import java.io.Serializable;

/**
 * Parrot class
 */
public class Parrot extends Bird implements Herbivore, Serializable {
    static final long serialVersionUID = 1;
    /**
     * @param name         Bird's name
     * @param age          Bird's age
     * @param featherColor color of bird's feathers
     */
    public Parrot(String name, int age, String featherColor) {
        super(name, age, featherColor);
    }

    public Parrot() {
    }

    @Override
    public String getFeatherColor() {
        return super.getFeatherColor();
    }

    @Override
    public void setFeatherColor(String featherColor) {
        super.setFeatherColor(featherColor);
    }

    /**
     * Parrot's sound
     */
    public void sound(){
        System.out.println(getName() + ": I can imitate almost every sound! I'm a parrot! Screeek!");
    }

    /**
     * Parrot eat's seeds
     */
    @Override
    public void eatingPlants() {
        System.out.println(getName() + ": I'm eating plants");

    }
}
