package pl.codementors.models;

import java.io.Serializable;

/**
 *  Base animal class
 * @author Krzysztof Koper
 */

public abstract class Animal implements Serializable {
    private int age;
    private String name;

    /**
     *
     * @param name animal's name
     * @param age animal's age
     */

    public Animal(String name, int age) {
        this.age = age;
        this.name = name;
    }

    public Animal() {
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void eating(){
        System.out.println("I'm hungry! give me more!");
    }
}
