package pl.codementors.models;

/**
 * Interface which will be implemented by meat-eating animal
 */
public interface Carnivore {
    /**
     * Carnivore eat meat
     */
    default void meatEating(){

    }
}
