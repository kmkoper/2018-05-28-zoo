package pl.codementors.models;

import java.io.Serializable;

/**
 *  Reptile class
 */
public class Reptile extends Animal implements Serializable {
    private String scaleColor;

    /**
     *
     * @param name Reptile's name
     * @param age Reptile's age
     * @param scaleColor color of reptile's scales
     */
    public Reptile(String name, int age, String scaleColor) {
        super(name, age);
        this.scaleColor = scaleColor;
    }

    public Reptile() {
    }

    public String getScaleColor() {
        return scaleColor;
    }

    public void setScaleColor(String scaleColor) {
        this.scaleColor = scaleColor;
    }

    @Override
    public int getAge() {
        return super.getAge();
    }

    @Override
    public void setAge(int age) {
        super.setAge(age);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public void eating() {
        super.eating();
    }
}
