package pl.codementors.models;

import java.io.Serializable;

/**
 * Iguana class
 */
public class Iguana extends Reptile implements Herbivore, Serializable {
    static final long serialVersionUID = 1;
    /**
     * @param name       Iguana's name
     * @param age        Iguana's age
     * @param scaleColor color of Iguana's scales
     */
    public Iguana(String name, int age, String scaleColor) {
        super(name, age, scaleColor);
    }

    public Iguana() {
    }


    /**
     * Iguana's sound
     */
    public void sound(){
        System.out.println(getName() + ": ssssssssss!!");
    }

    /**
     * Iguana's food are mostly leaves
     */
    @Override
    public void eatingPlants() {
        System.out.println(getName() + ": I'm eating plants.");
    }

}
