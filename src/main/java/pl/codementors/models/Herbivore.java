package pl.codementors.models;

/**
 * Interface which will be implemented by plant-eating animals
 */

public interface Herbivore {
    /**
     * Herbivore eat plants
     */
    default void eatingPlants(){
    }
}
