package pl.codementors.models;

import java.io.Serializable;

/**
 * Mammal class
 */
public class Mammal extends Animal implements Serializable {
    private String furColor;

    /**
     *
     * @param name mammal's name
     * @param age mammal's age
     * @param furColor color of mammal's fur
     */

    public Mammal(String name, int age, String furColor) {
        super(name, age);
        this.furColor = furColor;
    }

    public Mammal() {
    }

    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }

    @Override
    public int getAge() {
        return super.getAge();
    }

    @Override
    public void setAge(int age) {
        super.setAge(age);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public void eating() {
        super.eating();
    }
}
