package pl.codementors.models;

import java.io.Serializable;

/**
 * Bird class
 */
public class Bird extends Animal implements Serializable{
    private String featherColor;

    /**
     *
     * @param name Bird's name
     * @param age Bird's age
     * @param featherColor color of bird's feathers
     */
    public Bird(String name, int age, String featherColor) {
        super(name, age);
        this.featherColor = featherColor;
    }

    public Bird() {
    }

    public String getFeatherColor() {
        return featherColor;
    }

    public void setFeatherColor(String featherColor) {
        this.featherColor = featherColor;
    }

    @Override
    public int getAge() {
        return super.getAge();
    }

    @Override
    public void setAge(int age) {
        super.setAge(age);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public void eating() {
        super.eating();
    }
}
