package pl.codementors.models;

import java.io.Serializable;

/**
 * Wolf class.
 */
public class Wolf extends Mammal implements Carnivore, Serializable {
    static final long serialVersionUID = 1;
    /**
     * @param name     Wolf's name
     * @param age      Wolf's age
     * @param furColor color of Wolf's fur
     */
    public Wolf(String name, int age, String furColor) {
        super(name, age, furColor);
    }

    public Wolf() {
    }

    /**
     * Wolf's sound
     */

    public void sound() {
        System.out.println(getName() + ": aaaauuuuuuu!!");
    }

    /**
     * Wolf's are well known meat-lovers
     */
    @Override
    public void meatEating() {
        System.out.println(getName() + ": mmmmm I'm eating yummy meat!");
    }

}
